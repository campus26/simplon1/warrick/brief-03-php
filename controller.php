<?php
require_once('models/Utilisateur.php');

function home(){
    require_once('views/layout.content.php');
}

function pageread($page){
    $utilisateurs = read($page);
    $num_utilisateurs = userread();
    $records_per_page = recordmax();
    require_once('views/read.php');
}

function createcontrol($id,$nom,$prenom,$asresse){
    $msg = recupMSG();
    create($id,$nom,$prenom,$asresse);
    header('location: /newbookmark/read/1');
}

function updatecontrol($id){
    $utilisateur = recupuser($id);
    require_once('views/update.php');
}

function modifiercontrol($id,$nom,$prenom,$adresse){
    update($id,$nom,$prenom,$adresse);
    header('location: /newbookmark/read/1');
}

function deletecontrol($id){
    $utilisateur = recupuser($id);
    require_once('views/delete.php');
}

function confirmdelete($id){
    delete($id);
    header('location: /newbookmark/read/1');
}
?>

